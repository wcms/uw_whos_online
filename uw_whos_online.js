jQuery(document).ready(function($) {
  // Close "other users" by default.
  $('#uw_other h3 span:first-child').html('&#9656;');
  $('#uw_other ul').hide();
  // Since we're making the headers visible, wrap them in a button.
  $('#block-uw-whos-online-uw-online h3').wrapInner('<button></button>');
  // Add appropriate ARIA attributes for default states.
  $('#uw_privileged button').attr('aria-expanded', 'true');
  $('#uw_other button').attr('aria-expanded', 'false');
  // Handle show/hide.
  $('#block-uw-whos-online-uw-online button').on('click', function() {
    $list = $(this).closest('div').find('ul');
    if ($list.is(":visible")) {
      $list.hide();
      $('span:first-child',this).html('&#9656;');
      $(this).attr('aria-expanded', 'false');
    } else {
      $list.show();
      $('span:first-child',this).html('&#9662;');
      $(this).attr('aria-expanded', 'true');
    }
  });
});

